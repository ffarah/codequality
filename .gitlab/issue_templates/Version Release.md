## TODO

- [ ] Increment VERSION
- [ ] Make sure the x.y.z part of VERSION matches the default CodeClimate version in `run.sh`
- [ ] Move all CHANGELOG entries from `master (unreleased)` into a new section matching VERSION
- [ ] Merge VERSION and CHANGELOG changes to `master` with **no other changes**
- [ ] Create a tag on master matching version and push to `gitlab-org/ci-cd/codequality`
- [ ] Verify `release-version` succeeds in the tag pipeline
- [ ] Open a merge request in [GitLab](https://gitlab.com/gitlab-org/gitlab/merge_requests/new) against https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml to use the new version
- [ ] Close this issue
